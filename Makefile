c:
	gcc -o bin/hello_c hello.c
	./bin/hello_c

cpp:
	g++ -o bin/hello_cpp hello_cpp
	./bin/hello_cpp

cs:
	dotnet build hello.cs -o bin/hello_cs
	./bin/hello_cs

go:
	go build -o bin/hello_go hello.go
	./bin/hello_go

java:
	java hello.java

js:
	node hello.js

py:
	python hello.py

rs:
	rustc hello.rs -o bin/hello_rs
	./bin/hello_rs

sh:
	bash hello.sh

ts:
	node hello.ts
